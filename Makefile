include .env

all: | include net build install info

include:
ifeq ($(strip $(COMPOSE_PROJECT_NAME)),projectname)
#todo: ask user to make a project name and mv folders.
$(error Project name can not be default, please edit ".env" and set COMPOSE_PROJECT_NAME variable.)
endif

build: clean
	mkdir -p docroot
#	mkdir -p db/${COMPOSE_PROJECT_NAME}

install:
	@echo "Updating containers..."
	docker-compose pull
	@echo "Build and run containers..."
	docker-compose up -d
	docker-compose exec -T php composer global require -o --update-no-dev --no-suggest hirak/prestissimo
	docker-compose exec -T php sh -c "cd ../ && composer install --prefer-dist --optimize-autoloader"
	make -s si
	make chown

si:
	echo "Installing site $(COMPOSE_PROJECT_NAME)"
	docker-compose exec -T php drush --root=/var/www/html/docroot si lightning --db-url=mysql://d8:d8@mysql/d8 --account-pass=admin --site-name="$(SITE_NAME)" -y
	docker-compose exec -T php drush --root=/var/www/html/docroot en $(PROFILE_NAME) -y
	docker-compose exec -T php drush --root=/var/www/html/docroot updb -y
#	make -s front; 
#	make -s chown;

chown:
# Use this goal to set permissions in docker container
	docker-compose exec -T php /bin/sh -c "chown $(shell id -u):$(shell id -g) /var/www/html -R"
# Need this to fix files folder
	docker-compose exec -T php /bin/sh -c "chown www-data: /var/www/html/docroot/sites/default/files -R"
info:
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(COMPOSE_PROJECT_NAME)_web 2> /dev/null),true)
        @echo Project IP: $(shell docker inspect --format='{{.NetworkSettings.Networks.$(COMPOSE_PROJECT_NAME)_front.IPAddress}}' $(COMPOSE_PROJECT_NAME)_web)
endif
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(COMPOSE_PROJECT_NAME)_adminer 2> /dev/null),true)
        @echo Adminer IP: $(shell docker inspect --format='{{.NetworkSettings.Networks.$(COMPOSE_PROJECT_NAME)_front.IPAddress}}' $(COMPOSE_PROJECT_NAME)_adminer)
endif

exec:
	docker-compose exec php ash \

front:
	@echo "Building front tasks..."
	docker pull acidaniel/frontend:susy
	docker run --rm -v $(shell pwd)/src/$(PROFILE_NAME)/themes/$(THEME_NAME):/work acidaniel/frontend:susy bower install --allow-root
	docker run --rm -v $(shell pwd)/src/$(PROFILE_NAME)/themes/$(THEME_NAME):/work acidaniel/frontend:susy

phpcs:
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                -v $(shell pwd)/src/modules:/work/modules \
                -v $(shell pwd)/src/themes:/work/themes \
                -v $(shell pwd)/src/drush_make:/work/make \
                acidaniel/php7:sniffers phpcs -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=php,module,inc,install,profile,theme,yml \
                --ignore=*.css,*.md,*.js .
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                -v $(shell pwd)/src/modules:/work/modules \
                -v $(shell pwd)/src/themes:/work/themes \
                acidaniel/php7:sniffers phpcs -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=js \
                --ignore=*.css,*.md,libraries/*,styleguide/* .
phpcbf:
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                -v $(shell pwd)/src/modules:/work/modules \
                -v $(shell pwd)/src/themes:/work/themes \
                -v $(shell pwd)/src/drush_make:/work/make \
                acidaniel/php-7:sniffers phpcbf -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=php,module,inc,install,profile,theme,yml,txt,md \
                --ignore=*.css,*.md,*.js .
	docker run --rm \
                -v $(shell pwd)/src/$(PROFILE_NAME):/work/profile \
                -v $(shell pwd)/src/modules:/work/modules \
                -v $(shell pwd)/src/themes:/work/themes \
                acidaniel/php7:sniffers phpcbf -s --colors \
                --standard=Drupal,DrupalPractice \
                --extensions=js \
                --ignore=*.css,*.md,libraries/*,styleguide/* .
clean: info
	@echo "Removing networks for $(COMPOSE_PROJECT_NAME)"
ifeq ($(shell docker inspect --format="{{ .State.Running }}" $(COMPOSE_PROJECT_NAME)_web 2> /dev/null),true)
	docker-compose down
endif

iprange:
	$(shell grep -q -F 'IPRANGE=' .env || echo "\nIPRANGE=$(shell docker network inspect $(COMPOSE_PROJECT_NAME)_front --format '{{(index .IPAM.Config 0).Subnet}}')" >> .env)

net:
ifeq ($(strip $(shell docker network ls | grep $(COMPOSE_PROJECT_NAME))),)
	docker network create $(COMPOSE_PROJECT_NAME)_front
endif
	@make -s iprange

