(function($) {
  $("body").on("click", ".Search .form-item", function(e) {
    var x = e.offsetX;
    var y = e.offsetY;
    var w = $(window).width();
    console.log(w);

    var hideMenu = function() {
      $("#block-projectmenu").hide();
      $("#block-clearlinux-theme-branding").hide();
      $("#block-clearlinuxprojectnameblock").hide();
    }

    var showMenu = function() {
      $("#block-projectmenu").show();
      $("#block-clearlinux-theme-branding").show();
      $("#block-clearlinuxprojectnameblock").show();
    }

    if(x >= -39 && x <= -10 && y >= 15 && y <=35) {
      if(w <= 610) {
        hideMenu();
      }
      if(!$(this).hasClass('active')) {
        $(this).addClass('active');
      }
    }

    if(x >= 0 && x <= 31 && y >= 15 && y <=35) {
      if($(this).hasClass('active')) {
        $("#search-block-form").submit();
      }
    }

    if(x >= 291 && x <= 318 && y >= 15 && y <=35) {
      if($(this).hasClass('active')) {
        $(this).removeClass('active');
        $("#edit-keys").val("");
        showMenu();
      }
    }

  });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzZWFyY2gvc2VhcmNoLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigkKSB7XG4gICQoXCJib2R5XCIpLm9uKFwiY2xpY2tcIiwgXCIuU2VhcmNoIC5mb3JtLWl0ZW1cIiwgZnVuY3Rpb24oZSkge1xuICAgIHZhciB4ID0gZS5vZmZzZXRYO1xuICAgIHZhciB5ID0gZS5vZmZzZXRZO1xuICAgIHZhciB3ID0gJCh3aW5kb3cpLndpZHRoKCk7XG4gICAgY29uc29sZS5sb2codyk7XG5cbiAgICB2YXIgaGlkZU1lbnUgPSBmdW5jdGlvbigpIHtcbiAgICAgICQoXCIjYmxvY2stcHJvamVjdG1lbnVcIikuaGlkZSgpO1xuICAgICAgJChcIiNibG9jay1jbGVhcmxpbnV4LXRoZW1lLWJyYW5kaW5nXCIpLmhpZGUoKTtcbiAgICAgICQoXCIjYmxvY2stY2xlYXJsaW51eHByb2plY3RuYW1lYmxvY2tcIikuaGlkZSgpO1xuICAgIH1cblxuICAgIHZhciBzaG93TWVudSA9IGZ1bmN0aW9uKCkge1xuICAgICAgJChcIiNibG9jay1wcm9qZWN0bWVudVwiKS5zaG93KCk7XG4gICAgICAkKFwiI2Jsb2NrLWNsZWFybGludXgtdGhlbWUtYnJhbmRpbmdcIikuc2hvdygpO1xuICAgICAgJChcIiNibG9jay1jbGVhcmxpbnV4cHJvamVjdG5hbWVibG9ja1wiKS5zaG93KCk7XG4gICAgfVxuXG4gICAgaWYoeCA+PSAtMzkgJiYgeCA8PSAtMTAgJiYgeSA+PSAxNSAmJiB5IDw9MzUpIHtcbiAgICAgIGlmKHcgPD0gNjEwKSB7XG4gICAgICAgIGhpZGVNZW51KCk7XG4gICAgICB9XG4gICAgICBpZighJCh0aGlzKS5oYXNDbGFzcygnYWN0aXZlJykpIHtcbiAgICAgICAgJCh0aGlzKS5hZGRDbGFzcygnYWN0aXZlJyk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYoeCA+PSAwICYmIHggPD0gMzEgJiYgeSA+PSAxNSAmJiB5IDw9MzUpIHtcbiAgICAgIGlmKCQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XG4gICAgICAgICQoXCIjc2VhcmNoLWJsb2NrLWZvcm1cIikuc3VibWl0KCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYoeCA+PSAyOTEgJiYgeCA8PSAzMTggJiYgeSA+PSAxNSAmJiB5IDw9MzUpIHtcbiAgICAgIGlmKCQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XG4gICAgICAgICQodGhpcykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgICAgICAkKFwiI2VkaXQta2V5c1wiKS52YWwoXCJcIik7XG4gICAgICAgIHNob3dNZW51KCk7XG4gICAgICB9XG4gICAgfVxuXG4gIH0pO1xufSkoalF1ZXJ5KTtcbiJdLCJmaWxlIjoic2VhcmNoL3NlYXJjaC5qcyJ9
