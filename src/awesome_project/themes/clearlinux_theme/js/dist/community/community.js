(function($) {
  $(document).ready(function() {
    $(".Wrapper.community .container").addClass("owl-carousel owl-theme");

    $(".Wrapper.community .container").owlCarousel({
      autoplay: false,
      autoplayTimeout:6000,
      autoplayHoverPause:true,
      dots: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true,
              loop:true
          },
          600:{
              items:2,
              nav:true,
              loop:true
          },
          1000:{
              items:4,
              nav:true,
              loop:false
          }
      }
    });
  });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21tdW5pdHkvY29tbXVuaXR5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigkKSB7XG4gICQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAgICQoXCIuV3JhcHBlci5jb21tdW5pdHkgLmNvbnRhaW5lclwiKS5hZGRDbGFzcyhcIm93bC1jYXJvdXNlbCBvd2wtdGhlbWVcIik7XG5cbiAgICAkKFwiLldyYXBwZXIuY29tbXVuaXR5IC5jb250YWluZXJcIikub3dsQ2Fyb3VzZWwoe1xuICAgICAgYXV0b3BsYXk6IGZhbHNlLFxuICAgICAgYXV0b3BsYXlUaW1lb3V0OjYwMDAsXG4gICAgICBhdXRvcGxheUhvdmVyUGF1c2U6dHJ1ZSxcbiAgICAgIGRvdHM6IHRydWUsXG4gICAgICByZXNwb25zaXZlQ2xhc3M6dHJ1ZSxcbiAgICAgIHJlc3BvbnNpdmU6e1xuICAgICAgICAgIDA6e1xuICAgICAgICAgICAgICBpdGVtczoxLFxuICAgICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgICAgbG9vcDp0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICA2MDA6e1xuICAgICAgICAgICAgICBpdGVtczoyLFxuICAgICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgICAgbG9vcDp0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICAxMDAwOntcbiAgICAgICAgICAgICAgaXRlbXM6NCxcbiAgICAgICAgICAgICAgbmF2OnRydWUsXG4gICAgICAgICAgICAgIGxvb3A6ZmFsc2VcbiAgICAgICAgICB9XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xufSkoalF1ZXJ5KTtcbiJdLCJmaWxlIjoiY29tbXVuaXR5L2NvbW11bml0eS5qcyJ9
