(function($) {
  $(document).ready(function() {
    $(".Top__container").owlCarousel({
      autoplay: false,
      dots: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true,
              loop:true
          },
          768:{
              items:2,
              nav:true,
              loop:true
          },
          992:{
              items:3,
              nav:true,
              loop:false
          }
      }
    });
  });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ0b3Bfc3Rvcmllcy90b3Bfc3Rvcmllcy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oJCkge1xuICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKFwiLlRvcF9fY29udGFpbmVyXCIpLm93bENhcm91c2VsKHtcbiAgICAgIGF1dG9wbGF5OiBmYWxzZSxcbiAgICAgIGRvdHM6IHRydWUsXG4gICAgICByZXNwb25zaXZlQ2xhc3M6dHJ1ZSxcbiAgICAgIHJlc3BvbnNpdmU6e1xuICAgICAgICAgIDA6e1xuICAgICAgICAgICAgICBpdGVtczoxLFxuICAgICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgICAgbG9vcDp0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICA3Njg6e1xuICAgICAgICAgICAgICBpdGVtczoyLFxuICAgICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgICAgbG9vcDp0cnVlXG4gICAgICAgICAgfSxcbiAgICAgICAgICA5OTI6e1xuICAgICAgICAgICAgICBpdGVtczozLFxuICAgICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgICAgbG9vcDpmYWxzZVxuICAgICAgICAgIH1cbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG59KShqUXVlcnkpO1xuIl0sImZpbGUiOiJ0b3Bfc3Rvcmllcy90b3Bfc3Rvcmllcy5qcyJ9
