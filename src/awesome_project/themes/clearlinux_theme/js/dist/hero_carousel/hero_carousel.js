(function($) {
  $(document).ready(function() {
    $(".Carousel").owlCarousel({
      autoplay: true,
      autoplayTimeout:6000,
      autoplayHoverPause:true,
      dots: false,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true,
              loop:true
          },
          600:{
              items:1,
              nav:true,
              loop:true
          },
          1000:{
              items:1,
              nav:true,
              loop:true
          }
      }
    });
  });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJoZXJvX2Nhcm91c2VsL2hlcm9fY2Fyb3VzZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCQpIHtcbiAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgJChcIi5DYXJvdXNlbFwiKS5vd2xDYXJvdXNlbCh7XG4gICAgICBhdXRvcGxheTogdHJ1ZSxcbiAgICAgIGF1dG9wbGF5VGltZW91dDo2MDAwLFxuICAgICAgYXV0b3BsYXlIb3ZlclBhdXNlOnRydWUsXG4gICAgICBkb3RzOiBmYWxzZSxcbiAgICAgIHJlc3BvbnNpdmVDbGFzczp0cnVlLFxuICAgICAgcmVzcG9uc2l2ZTp7XG4gICAgICAgICAgMDp7XG4gICAgICAgICAgICAgIGl0ZW1zOjEsXG4gICAgICAgICAgICAgIG5hdjp0cnVlLFxuICAgICAgICAgICAgICBsb29wOnRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIDYwMDp7XG4gICAgICAgICAgICAgIGl0ZW1zOjEsXG4gICAgICAgICAgICAgIG5hdjp0cnVlLFxuICAgICAgICAgICAgICBsb29wOnRydWVcbiAgICAgICAgICB9LFxuICAgICAgICAgIDEwMDA6e1xuICAgICAgICAgICAgICBpdGVtczoxLFxuICAgICAgICAgICAgICBuYXY6dHJ1ZSxcbiAgICAgICAgICAgICAgbG9vcDp0cnVlXG4gICAgICAgICAgfVxuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn0pKGpRdWVyeSk7XG4iXSwiZmlsZSI6Imhlcm9fY2Fyb3VzZWwvaGVyb19jYXJvdXNlbC5qcyJ9
