(function($) {
  $(document).ready(function() {
    var cboxOptions = {
      width: "100%",
      height: "100%",
      maxWidth: "853px",
      maxHeight: "480px",
      iframe: true,
      scrolling: false,
      closeButton: true
    }

    $("body").on("click touchstart", ".Video__play", function(event) {
        event.preventDefault();
        var videoUrl = $(this).data("url") + '?autoplay=1&modestbranding=1&autohide=1&showinfo=0&controls=0';
        $(".youtube").colorbox(cboxOptions);
    });

    $(window).resize(function(){
      $.colorbox.resize({
        width: window.innerWidth > parseInt(cboxOptions.maxWidth) ? cboxOptions.maxWidth : cboxOptions.width,
        height: window.innerHeight > parseInt(cboxOptions.maxHeight) ? cboxOptions.maxHeight : cboxOptions.height
      });
    });

    $(document).on('cbox_open',function(){
      $(document.body).css('overflow','hidden');
    }).on('cbox_closed',function(){
      $(document.body).css('overflow','');
    });

  });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJoZXJvX3ZpZGVvL2hlcm9fdmlkZW8uanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCQpIHtcbiAgJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24oKSB7XG4gICAgdmFyIGNib3hPcHRpb25zID0ge1xuICAgICAgd2lkdGg6IFwiMTAwJVwiLFxuICAgICAgaGVpZ2h0OiBcIjEwMCVcIixcbiAgICAgIG1heFdpZHRoOiBcIjg1M3B4XCIsXG4gICAgICBtYXhIZWlnaHQ6IFwiNDgwcHhcIixcbiAgICAgIGlmcmFtZTogdHJ1ZSxcbiAgICAgIHNjcm9sbGluZzogZmFsc2UsXG4gICAgICBjbG9zZUJ1dHRvbjogdHJ1ZVxuICAgIH1cblxuICAgICQoXCJib2R5XCIpLm9uKFwiY2xpY2sgdG91Y2hzdGFydFwiLCBcIi5WaWRlb19fcGxheVwiLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB2YXIgdmlkZW9VcmwgPSAkKHRoaXMpLmRhdGEoXCJ1cmxcIikgKyAnP2F1dG9wbGF5PTEmbW9kZXN0YnJhbmRpbmc9MSZhdXRvaGlkZT0xJnNob3dpbmZvPTAmY29udHJvbHM9MCc7XG4gICAgICAgICQoXCIueW91dHViZVwiKS5jb2xvcmJveChjYm94T3B0aW9ucyk7XG4gICAgfSk7XG5cbiAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uKCl7XG4gICAgICAkLmNvbG9yYm94LnJlc2l6ZSh7XG4gICAgICAgIHdpZHRoOiB3aW5kb3cuaW5uZXJXaWR0aCA+IHBhcnNlSW50KGNib3hPcHRpb25zLm1heFdpZHRoKSA/IGNib3hPcHRpb25zLm1heFdpZHRoIDogY2JveE9wdGlvbnMud2lkdGgsXG4gICAgICAgIGhlaWdodDogd2luZG93LmlubmVySGVpZ2h0ID4gcGFyc2VJbnQoY2JveE9wdGlvbnMubWF4SGVpZ2h0KSA/IGNib3hPcHRpb25zLm1heEhlaWdodCA6IGNib3hPcHRpb25zLmhlaWdodFxuICAgICAgfSk7XG4gICAgfSk7XG5cbiAgICAkKGRvY3VtZW50KS5vbignY2JveF9vcGVuJyxmdW5jdGlvbigpe1xuICAgICAgJChkb2N1bWVudC5ib2R5KS5jc3MoJ292ZXJmbG93JywnaGlkZGVuJyk7XG4gICAgfSkub24oJ2Nib3hfY2xvc2VkJyxmdW5jdGlvbigpe1xuICAgICAgJChkb2N1bWVudC5ib2R5KS5jc3MoJ292ZXJmbG93JywnJyk7XG4gICAgfSk7XG5cbiAgfSk7XG59KShqUXVlcnkpO1xuIl0sImZpbGUiOiJoZXJvX3ZpZGVvL2hlcm9fdmlkZW8uanMifQ==
