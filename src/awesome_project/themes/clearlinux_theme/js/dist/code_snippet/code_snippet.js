(function($) {
  $("body").on("click", ".hljs", function(e) {
    var x = e.offsetX;
    var y = e.offsetY;

    if(x >= 0 && x <= 30 && y >= 0 && y <=30) {
      $(this).addClass('active');

      setTimeout(function() {
        $(this).removeClass('active');
      }, 500);
    }
  });
})(jQuery);

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb2RlX3NuaXBwZXQvY29kZV9zbmlwcGV0LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigkKSB7XG4gICQoXCJib2R5XCIpLm9uKFwiY2xpY2tcIiwgXCIuaGxqc1wiLCBmdW5jdGlvbihlKSB7XG4gICAgdmFyIHggPSBlLm9mZnNldFg7XG4gICAgdmFyIHkgPSBlLm9mZnNldFk7XG5cbiAgICBpZih4ID49IDAgJiYgeCA8PSAzMCAmJiB5ID49IDAgJiYgeSA8PTMwKSB7XG4gICAgICAkKHRoaXMpLmFkZENsYXNzKCdhY3RpdmUnKTtcblxuICAgICAgc2V0VGltZW91dChmdW5jdGlvbigpIHtcbiAgICAgICAgJCh0aGlzKS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XG4gICAgICB9LCA1MDApO1xuICAgIH1cbiAgfSk7XG59KShqUXVlcnkpO1xuIl0sImZpbGUiOiJjb2RlX3NuaXBwZXQvY29kZV9zbmlwcGV0LmpzIn0=
