(function() {
  //Show menu
  $("body").on("click touchstart", "#ico-open-menu", function(e) {
    e.preventDefault();
    $("body").find(".Header__nav-submenu").toggleClass('active');
    $("html").toggleClass("active");
  });

  //Hide menu
  var hideMenu = function() {
    $("body").find(".Header__nav-submenu").toggleClass('active');
    $("html").removeClass("active");
  }

  $("body").on("click touchstart", "#ico-close-menu", function(e) {
    e.preventDefault();
    hideMenu();
  });

  $("body").click(function(e) {
    console.log(e);
    if($(".Header__nav-submenu_technologies").hasClass("active") && e.target.id != "ico-open-menu" && e.target.className != "fa fa-bars" ) {
      hideMenu();
    }
  });

  //expand submenu
  $("body").on("click touchstart", ".submenu_header", function(e) {
    e.preventDefault();
    $("body").find(".Header__nav-submenu.active li:first-child ul li a").removeClass("active");
    $(this).toggleClass("active");
  });

  $(document).keyup(function(e) {
    if (e.keyCode == 27 && $(".Header__nav-submenu_technologies").hasClass("active")) {
      hideMenu();
    }
  });

})();
