(function($) {
  $("body").on("click", ".hljs", function(e) {
    var x = e.offsetX;
    var y = e.offsetY;

    if(x >= 0 && x <= 30 && y >= 0 && y <=30) {
      $(this).addClass('active');

      setTimeout(function() {
        $(this).removeClass('active');
      }, 500);
    }
  });
})(jQuery);
