(function($) {
  $(document).ready(function() {
    $(".Top__container").owlCarousel({
      autoplay: false,
      dots: true,
      responsiveClass:true,
      responsive:{
          0:{
              items:1,
              nav:true,
              loop:true
          },
          768:{
              items:2,
              nav:true,
              loop:true
          },
          992:{
              items:3,
              nav:true,
              loop:false
          }
      }
    });
  });
})(jQuery);
